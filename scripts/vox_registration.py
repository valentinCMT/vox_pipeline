import os, sys

sys.path.append("/home/valentin/Vox_pipeline/keras_med")
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # disable tensorflow warnings
import argparse
import glob
import time
import numpy as np
import voxelmorph as vxm
from scipy.interpolate import interpn
from scipy import ndimage, misc
import nibabel as nib
import SimpleITK as sitk
from keras_med_io.inference.infer_utils import pad_nonint_extraction, undo_reshape_padding

from io_img_only import LocalPreprocessingBinarySeg
from io_utils import resize_data, multi_channel_labels, nii_to_np, resize_and_uncrop, hist_match


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="For preprocessing the dataset")
    parser.add_argument("--fixed_img", type = str, required = True,
                        help = "Path to the fixed image")
    parser.add_argument("--fixed_label", type = str, required = False,
                        help = "Path to the fixed label")
    parser.add_argument("--moving_img", type = str, required = True,
                        help = "Path to the moving image")
    parser.add_argument("--moving_label", type = str, required = False,
                        help = "Path to the moving label")
    parser.add_argument("--pre_dir", type = str, required = True,
                        help = "Path to the directory where the preprocessed data are saved")
    parser.add_argument("--npz_dir", type = str, required = False,
                        help = "Path to the directory where the npz data are saved")
    parser.add_argument("--reshape", type = int, required = False, default = 128,
                        help = "size x of the reshaping (new shape = (x,x,x), x must be a multiple of 16.")
    parser.add_argument("--layer_size", type = int, required = False, default = 32,
                        help = "size of the layers")
    parser.add_argument("--weights_dir", type = str, required = True,
                        help = "Path to the base directory where the trained weights are")
    parser.add_argument("--std_norm", type = float, required = False, default = .2,
                        help = "value of the std normalization")
    parser.add_argument("--output_dir", type = str, required = True,
                        help = "Path to the directory where the results are saved")

    ########################## PREPROCESSING ############################

    args = parser.parse_args()

    t1 = time.process_time()
    
    # getting the files names

    moving_img_file = str(args.moving_img).split("/")[-1]
    id_moving = moving_img_file.split(".")[0].split("-")[1]
    moving_label_file = str(args.moving_label).split("/")[-1]
    fixed_img_file = str(args.fixed_img).split("/")[-1]
    id_fixed = fixed_img_file.split(".")[0].split("-")[1]
    if args.fixed_label is not None:
        fixed_label_file = str(args.fixed_label).split("/")[-1]

    # get vox spacing and origin using sitk (it's a pain in the ass with nibabel)

    fixed_sitk = sitk.ReadImage(args.fixed_img)
    spacing = fixed_sitk.GetSpacing()
    origin = fixed_sitk.GetOrigin()

    fixed_nib = nib.load(args.fixed_img)
    old_shape_fixed = nii_to_np(fixed_nib).shape

    # cropping + normalizing, save the cropping and original spacing to reconstruct the image and seg in the postprocessing"
    
    preprocess_moving = LocalPreprocessingBinarySeg(args.moving_img, args.pre_dir, args.moving_label)
    preprocess_fixed = LocalPreprocessingBinarySeg(args.fixed_img, args.pre_dir, args.fixed_label)
    
    # returning the preprocessed data
    
    print("\n \nPreprocessing %s and %s" %(fixed_img_file, moving_img_file))

    preprocessed_moving_img, preprocessed_moving_label, spacing_moving, coords_moving = preprocess_moving.gen_data()

    
    if args.fixed_label is not None:
        preprocessed_fixed_img, preprocessed_fixed_label, spacing_fixed, coords_fixed = preprocess_fixed.gen_data()
    else:
        preprocessed_fixed_img, spacing_fixed, coords_fixed = preprocess_fixed.gen_data()
    

    print("\n \nSaved preprocessed volumes and segmentation in %s" %(str(args.pre_dir)))

    # Resizing the data
    
    print("\nResizing the data to (%i,%i,%i)" %(args.reshape,args.reshape,args.reshape))
    print("%s volume" %fixed_img_file)
    resized_fixed_img = resize_data(preprocessed_fixed_img, args.reshape,args.reshape,args.reshape)
    print("%s volume" %moving_img_file)
    resized_moving_img = resize_data(preprocessed_moving_img, args.reshape,args.reshape,args.reshape)
    print("%s labels" %moving_label_file)
    resized_moving_label = resize_data(preprocessed_moving_label, args.reshape,args.reshape,args.reshape)
    if args.fixed_label is not None:
        print("%s labels" %fixed_label_file)
        resized_fixed_label = resize_data(preprocessed_fixed_label, args.reshape,args.reshape,args.reshape)


    # std normalization

    resized_fixed_img = resized_fixed_img/resized_fixed_img.std() * args.std_norm
    resized_moving_img = resized_moving_img/resized_moving_img.std() * args.std_norm

    
    # Converting to npz
    
    #print("\n \nConverting to .npz format")
    vol_moving ,seg_moving = resized_moving_img, resized_moving_label
    #np.savez(str(os.path.join(args.npz_dir, moving_img_file.split(".")[0] + ".npz")), vol=vol_moving, seg=seg_moving)
    
    if args.fixed_label is not None:
        vol_fixed ,seg_fixed = resized_fixed_img, resized_fixed_label
        #np.savez(str(os.path.join(args.npz_dir, fixed_img_file.split(".")[0] + ".npz")), vol=vol_fixed, seg=seg_fixed)
    else:
        vol_fixed = resized_fixed_img
        #np.savez(str(os.path.join(args.npz_dir, fixed_img_file.split(".")[0] + ".npz")), vol=vol_fixed, seg=None)

    #print("\n \nnpz files saved in %s" %args.npz_dir)
    
    # multi channel label

    nb_labels = int(seg_moving.max()+1)

    if args.fixed_label is not None:
        seg_moving_multi = multi_channel_labels(seg_moving, nb_labels)
        seg_fixed_multi = multi_channel_labels(seg_fixed, nb_labels)
    
    ########################## PREDICTING ############################
    
    # configure unet features
    
    vol_shape = vol_fixed.shape
    size = args.layer_size
    
    nb_features = [[size, size*2, size*2, size*2],[size*2, size*2, size*2, size*2, size*2, size, size]]

    vxm_model = vxm.networks.VxmDense(vol_shape, nb_features, int_steps=0)
    weights = vxm_model.load_weights(args.weights_dir)

    # add dims to the inputs

    fixed = np.reshape(vol_fixed, (1,) + vol_shape + (1,))
    moving = np.reshape(vol_moving, (1,) + vol_shape + (1,))

    # prediction

    t2 = time.process_time()

    print("\n \nPredicting...")
    val_input = [moving,fixed] # moving and fixed, in that order
    val_pred = vxm_model.predict(val_input)
    print("\n \nDone !")

    t3 = time.process_time()
    
    flow = val_pred[1].squeeze() # deformation field
    imgs = np.asarray([img[0, :, :, :, 0] for img in val_input + val_pred])
    warped = imgs[2,:,:,:]
    warped = hist_match(warped, vol_fixed)

    ########################## POSTPROCESSING ############################
    
    # Initializing the grid for the interpolation

    print("\n \nComputing the warped labels")
    xx = np.arange(vol_shape[1])
    yy = np.arange(vol_shape[0])
    zz = np.arange(vol_shape[2])
    grid = np.rollaxis(np.array(np.meshgrid(xx, yy, zz)), 0, 4)

    
    sample = np.zeros(flow.shape)
    sample[:,:,:,0] = flow[:,:,:,1] + grid[:,:,:,0]
    sample[:,:,:,1] = flow[:,:,:,0] + grid[:,:,:,1]
    sample[:,:,:,2] = flow[:,:,:,2] + grid[:,:,:,2]

    sample = np.stack((sample[:, :, :, 1], sample[:, :, :, 0], sample[:, :, :, 2]), 3)
    
    # warping the labels
    seg_warped = interpn((yy, xx, zz), seg_moving, sample, method='nearest', bounds_error=False, fill_value=0)
    
    # Resizing and un-cropping to match the input
    vol_warped_uncropped = resize_and_uncrop(warped, coords_fixed,old_shape_fixed)
    seg_warped_uncropped = resize_and_uncrop(seg_warped, coords_fixed,old_shape_fixed)
    # Smooting the labels with a median filter
    seg_warped_uncropped = ndimage.median_filter(seg_warped_uncropped, size=3)

    # computing the dice scores of each label (if the labels of the fixed images are given)
    if args.fixed_label is not None:

        seg_warped_multi = multi_channel_labels(seg_warped,nb_labels)
        seg_warped_multi_original_size = multi_channel_labels(seg_warped_uncropped,nb_labels)
        seg_fixed_uncropped = resize_and_uncrop(seg_fixed, coords_fixed,old_shape_fixed)
        seg_fixed_multi_original_size = multi_channel_labels(seg_fixed_uncropped,nb_labels)

        for y in range(nb_labels):
            dice_before = np.sum(seg_moving_multi[...,y].flatten()[seg_fixed_multi[...,y].flatten()==1])*2.0 / (np.sum(seg_fixed_multi[...,y].flatten()) + np.sum(seg_moving_multi[...,y].flatten()))
            dice_after = np.sum(seg_warped_multi[...,y].flatten()[seg_fixed_multi[...,y].flatten()==1])*2.0 / (np.sum(seg_fixed_multi[...,y].flatten()) + np.sum(seg_warped_multi[...,y].flatten()))
            dice_after_original_size = np.sum(seg_warped_multi_original_size[...,y].flatten()[seg_fixed_multi_original_size[...,y].flatten()==1])*2.0 / (np.sum(seg_fixed_multi_original_size[...,y].flatten()) + np.sum(seg_warped_multi_original_size[...,y].flatten()))

            print("LABEL %i" %y)
            print("\n dice before : %.4f" %dice_before)
            print("\n dice after : %.4f" %dice_after)
            print("\n dice after original size : %.4f" %dice_after_original_size)

    # converting to .nii
    vol_warped_nii = sitk.GetImageFromArray(vol_warped_uncropped)
    seg_warped_nii = sitk.GetImageFromArray(seg_warped_uncropped)

    # set original voxel size and origin
    vol_warped_nii.SetSpacing(spacing)
    vol_warped_nii.SetOrigin(origin)
    seg_warped_nii.SetSpacing(spacing)
    seg_warped_nii.SetOrigin(origin)

    sitk.WriteImage(vol_warped_nii, os.path.join(args.output_dir,"result_img_" + id_moving + "_warped_to_" + id_fixed + ".nii"))
    sitk.WriteImage(seg_warped_nii, os.path.join(args.output_dir,"result_seg_" + id_moving + "_warped_to_" + id_fixed + ".nii"))

    t4 = time.process_time()

    print("\n \nSaving results in : ", str(args.output_dir))

    print("\n \nPreprocessing done in : %.4f s" %(t2-t1))
    print("\nRegistration done in : %.4f s" %(t3-t2))
    print("\nPostprocessing done in : %.4f s" %(t4-t3))
    




        
